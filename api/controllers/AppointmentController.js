/**
 * AppointmentController
 *
 * @description :: Server-side logic for managing appointments
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */


module.exports = {
	find: function(req, res){
        Appointment.find({
            status :{'!': ['canceled', 'finished']},
            or:[
                {provider: req.user.id}, 
                {client: req.user.id}
            ]

        }).populateAll().exec(function(err, found){
            return res.json(found); 
        });
    },

    accept: function(req, res){
        var id = req.param('id');

        User.findOne({id: req.user.id}).exec(function findOneCB(err,user){

            Appointment.update({id:id, provider: req.user.id}, {status: 'accepted'}).exec(function afterwards(err,updated){
                if(!updated[0]) return res.send({});

                /* send message */
                var msg = "El usuario " +user.firstName+ " ha aceptado la cita";
                msgHelper.send(req.user.id, updated[0].client, msg, function(err,created){
                    console.log(created);
                });

                return res.json(updated[0]); 
            })
        })
    },

    cancel: function(req, res){
        var id = req.param('id');
        var msg = req.param('msg');

        User.findOne({id: req.user.id}).exec(function findOneCB(err,user){

            Appointment.update({id:id, 
                or:[
                    {provider: req.user.id}, 
                    {client: req.user.id}
                ]}, {status: 'canceled'}).exec(function afterwards(err,updated){

                if(!updated[0]) return res.send({});

                /* send a message */
                if(!msg) msg = 'No se especifico ninguna razon';
                
                msg = "El usuario " +user.firstName+ " ha actualizado la fecha de la cita: " + msg;
                var to = (req.user.id == updated[0].provider) ? updated[0].client : req.user.id;
                msgHelper.send(req.user.id, to, msg, function(err,created){
                    console.log(created);
                });

                return res.json(updated[0]); 

            })
        })
    },
    

    update: function(req, res){
        var id = req.param('id');
        var when = req.param('when');
        var msg = req.param('msg');

        User.findOne({id: req.user.id}).exec(function findOneCB(err,user){

            Appointment.update({id:id, 
                or:[
                    {provider: req.user.id}, 
                    {client: req.user.id}
                ]}, {when: when}).exec(function afterwards(err,updated){

                if(!updated[0]) return res.send({});

                /* send a message */
                if(!msg) msg = 'No se especifico ninguna razon';

                msg = "El usuario " +user.firstName+ " ha actualizado la fecha de la cita: " + msg;
                var to = (req.user.id == updated[0].provider) ? updated[0].client : req.user.id;
                msgHelper.send(req.user.id, to, msg, function(err,created){
                    console.log(created);
                });

                return res.json(updated[0]); 

            })
        })
    }
};


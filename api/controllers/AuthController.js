/**
 * AuthController
 *
 * @description :: Server-side logic for managing auth
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var passport    = require('passport'),
    jwt         = require('jsonwebtoken'),
    settings    = sails.config.settings;

//http://iliketomatoes.com/implement-passport-js-authentication-with-sails-js-0-10-2/#disqus_thread
//https://github.com/kdelemme/nodejs-token-auth

module.exports = {

    login: function(req, res){
        passport.authenticate('local', function(err, user, info) {

            if ((err) || (!user)) {
                return res.send({
                    message: info.message,
                    user: user
                });
            }

            return res.send({
                user: user,
                token: info.token,
            })

        })(req, res);
    },


    logout: function(req, res){
        req.logout();
        res.redirect('/');
    },


};


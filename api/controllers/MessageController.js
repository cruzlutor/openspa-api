/**
 * UMessageController
 *
 * @description :: Server-side logic for managing messages
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    conversation: function(req, res){
        Message.find({where: {
            or : [
                { from: 1,  to:2},
                { from: 2,  to:1}
            ]
        }, sort: 'updatedAt ASC'}).populateAll().exec(function(err, found){
            return res.json(found); 
        });
    }

};


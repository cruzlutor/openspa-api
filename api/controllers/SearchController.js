/**
 * UserServiceController
 *
 * @description :: Server-side logic for managing Userservices
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	index: function (req, res) {
        console.log(req.user);
        var self = this;

        var slug = req.param('slug');

        Service.findOne({slug:slug}).exec(onSearchSlug);

        function onSearchSlug(e, r){
            if(r != null){
                UserService.find({service:r.id}).populate('service').populate('user').exec(onSearchList);
            }else{
                return res.send({error:true});
            }
        }

        function onSearchList(e, r){
            if(r != null){
                return res.send(r);
            }else{
                return res.send({error:true});
            }
        }
    },


};
/*
function onSearch(){
    console.log('call back');
}
*/
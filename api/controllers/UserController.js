/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var easyimg = require('easyimage'),
    fs = require('fs');

module.exports = {

    findOne: function(req, res){
        if(req.isAuthenticated()){
            return res.send(req.user);
        }else{
            return res.forbidden();
        }
    },

    update: function(req, res){
        var userId = req.param('id');
        if(req.user){
            if(req.user.id == userId){
                var params = req.allParams();
                User.update({id:req.user.id}, params).exec(function afterwards(err,updated){
                    return res.send(updated[0]);
                })
            }else{
                return res.forbidden();
            }
            
        }else{
            return res.forbidden();
        }
    },

    /**
     * Update the user avatar
     * @param  {[type]} req [description]
     * @param  {[type]} res [description]
     * @return {[type]}     [description]
     */
    updateAvatar: function(req, res){
        var avatar = req.file('avatar');

        avatar.upload({ dirname: '../../assets/images'}, function onUploadComplete(err, files) {    
            if (err) return res.serverError(err); 

            easyimg.info(files[0].fd).then(
                function(file) {

                    /* delete old images */
                    try {
                        fs.unlinkSync('assets/images/' + req.user.avatar);
                        fs.unlinkSync('assets/images/thumb-' + req.user.avatar)
                    }
                    catch (e) {
                        console.log('no file found');
                    } 

                    /* update user object */
                    User.update({id:req.user.id}, {avatar: file.name}).exec(function afterwards(err,updated){
                        
                        /* create a thumbnail */
                        easyimg.thumbnail({
                            src:file.path, dst:'assets/images/thumb-'+file.name,
                            width:100, height:100,
                        }).then(
                            function(thumb){
                                return res.send(updated[0]);
                            }, function(err){
                                return res.serverError(err); 
                            }
                        )
                    });

                }, function (err) {
                    return res.serverError(err); 
                }
            );
        });    
    }
};


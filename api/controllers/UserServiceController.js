/**
 * UserServiceController
 *
 * @description :: Server-side logic for managing Userservices
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var getList = function(user_id, next){
    UserService.find({user:user_id}).populate('service').populate('user').exec(next);
};

module.exports = {
    create: function(req, res){

        //create userservice record
        UserService.create(req.body).exec(onCreate);

        //if is a success create
        function onCreate(err, created){
            if (err) return res.serverError(err); 
            var uploadFile = req.file('image');

            uploadFile.upload({ dirname: '../../assets/images'}, function onUploadComplete(err, files) {    
                if (err) return res.serverError(err); 
                var parts = files[0].fd.split("\\");
                var fileName = parts[parts.length - 1];
                created.images = [fileName];
                created.save();
                res.send(created);
            });            
        }
    }
};

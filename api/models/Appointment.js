/**
* Appointment.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

    attributes: {
        when:{
            type: 'datetime'
        },

        service:{
            collection: 'UserService',
        },

        provider:{
            model: 'user'
        },

        client:{
            model: 'user'
        },

        place:{
            type: 'json'
        },

        status:{
            type: 'string',
            enum: ['pending', 'accepted', 'canceled', 'finished']
        }

    }
};


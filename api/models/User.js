/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcryptjs');

module.exports = {
    connection: 'localDiskDb',
    attributes: {

        email: { 
            type: 'email',  
            unique: true,
            required: true,
        },

        password: {
            type: 'string',
            required: true,
        },

        tokens: {
            collection:'UserToken',
            via: 'user'
        },

        firstName: {
            type: 'string',
            required: true,
        },

        lastName: {
            type: 'string',
        },

        avatar:{
            type: 'string',
        },

        gender:{
            type: 'string',
            enum: ['m', 'f'],
        },

        type:{
            type: 'string',
            enum: ['client', 'provider'],
            defaultsTo: 'client',
        },

        city:{
            model:'city'
        },

        phone:{
            type: 'string',
        },

        isStaff: { 
            type:'boolean',
            defaultsTo: false
        },

        isAdmin: { 
            type:'boolean',
            defaultsTo: false
        },

        isActive: { 
            type:'boolean',
            defaultsTo: false
        },

        isTrue: {
            type:'boolean',
            defaultsTo: false
        },

        // service m2m relation
        services:{
            collection: 'UserService',
            via: 'user'
        },

        // appointment as client o2m relation
        client:{
            collection: 'Appointment',
            via: 'client',
        },

        // appointment as provider o2m relation
        provider:{
            collection: 'Appointment',
            via: 'provider',
        }
    },

    // beforeCreate: function(values, cb){
    //     bcrypt.hash(values.password, 10, function(err, hash){
    //         if(err) return cb(err)
    //         values.password = hash;
    //         cb();
    //     })
    // },
};
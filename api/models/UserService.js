/**
* UserService.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

    attributes: {
        title: {
            type: 'string',
        },

        user: {
            model: 'user',
        },

        service:{
            model: 'service',
        },

        price:{
            type: 'integer',
        },

        images:{
            type: 'array',
        }

    }
};


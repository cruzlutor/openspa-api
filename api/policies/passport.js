/**
 * [exports description]
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */

var passport = require('passport');

module.exports = function (req, res, next) {
    passport.authenticate('bearer', {session: false}, function(err, user, info) {
        if(user) req.user = user;
        return next();
    })(req, res);
};
'user strict';

module.exports = {
    send: function(from, to, message, next){
        Message.create({from:from, to:to, message:message}).exec(next);
    },
}
'use strict';

/**
 * Token auth method
 */

var passport    = require('passport'),
    strategies  = sails.config.passport,
    settings    = sails.config.settings;

passport.use('bearer', new strategies.bearer.strategy(
    function(accessToken, done) {

        UserToken.findOne({token: accessToken}, function(err, token){
            if (err) return done(err);
            if (!token) return done(null, false, {message: 'Incorrect token.'});

            User.findOne(token.user, function(err, user) {

                return done(null, user, {
                    message: 'correct login', 
                    token: token
                });

            });
        })
    }
));


/**
 * user password auth method
 */

passport.use('local', new strategies.local.strategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function(email, password, done) {
        
        var UserTokenValidate = function(user, found, next){

            var token = tokenHelper.create({user: user.id});

            /* the token was not found */ 
            if(!found){
                UserToken.create({user: user.id, provider:'jwt', token: token}).exec(function createCB(err,created){
                    return next(err, token);
                });

            }else{

                tokenHelper.verify(found.token, function(err, decoded){

                    /* if get a issue with the token then create new */
                    if (err){
                        found.token = token;
                        found.save()
                    }

                    return next(null, found.token);
                });
            }
        }

        User.findOne({email:email}, function(err, user){
            if (err) return done(err); 
            if(!user) return done(null, false, {message: 'Incorrect email.'});
            if(user.password != password) return done(null, false, {message: 'Incorrect password.'});

            //find token
            UserToken.findOne({user: user.id, provider:'jwt'}).exec( function(err, found){
                if (err) return done(err); 
                UserTokenValidate(user, found, function(err, token){

                    return done(null, user, {
                        message: 'correct login', 
                        token: token
                    });
                });

            });
        });

    }
));

passport.serializeUser(function (user, next) {
  next(null, user.id);
});

passport.deserializeUser(function (id, next) {
  User.findOne(id, next);
});

'user strict';
var jwt         = require('jsonwebtoken'),
    settings    = sails.config.settings;

module.exports = {
    create: function(data){
        return jwt.sign(data, settings.token.key, {expiresInSeconds:settings.token.expiration});
    },

    verify: function(token, done){
        jwt.verify(token, settings.token.key, done);
    }
}